#include <stdint.h>

// USE_
#define USE_COMPAS true
#define USE_ACCEL false
#define USE_GPS false

// PRINT_
#define PRINT_COMPAS false
#define PRINT_ACCEL false
#define PRINT_GPS false


#if USE_COMPAS

#include "compass2_hw.h"

sbit COMPASS2_CS at GPIOD_ODR.B13;

float mRes;
uint8_t asax, asay, asaz;
float adjusted_ASAX, adjusted_ASAY, adjusted_ASAZ;
float heading, adjusted_MX, adjusted_MY, adjusted_MZ, magbias[3];
int16_t mx, my, mz;
char compass_text[20] = { 0 };

void compas_setup( bus_mode_t mode, uint8_t addr )
{
    // GPIOs
    GPIO_Digital_Output( &GPIOB_BASE, _GPIO_PINMASK_13 );

    // UART
    UART1_Init( 9600 );
    UART1_Write_Text( "UART Initialized\r\n" );

    // I2C
    I2C1_Init_Advanced( 100000, &_GPIO_MODULE_I2C1_PB67 );
    UART1_Write_Text( "I2C Initialized\r\n" );

    // Compass 2
    UART1_Write_Text( "Getting Device ID..." );
    compass2_hw_init( addr, mode );
    UART1_Write_Text( "Compass Initialized\r\n" );

    // Compass 2 setup
    mRes = compass2_set_scale_factor( RES_16 );
    magbias[0] = +470;
    magbias[1] = +120;
    magbias[2] = +125;

    compass2_get_self_test( &mx, &my, &mz );
    UART1_Write_Text( "x y z Values: " );
    LongWordToStr( mx, compass_text );
    UART1_Write_Text( compass_text );
    UART1_Write_Text( "\t" );
    LongWordToStr( my, compass_text );
    UART1_Write_Text( compass_text );
    UART1_Write_Text( "\t" );
    LongWordToStr( mz, compass_text );
    UART1_Write_Text( compass_text );
    UART1_Write_Text( "\r\n" );

    compass2_get_adjustment( &asax, &asay, &asaz );
    adjusted_ASAX = ( (float)asax - 128 ) / 256 + 1;
    adjusted_ASAY = ( (float)asay - 128 ) / 256 + 1;
    adjusted_ASAZ = ( (float)asaz - 128 ) / 256 + 1;

    compass2_set_mode( MODE_CONT_1 );
    compass2_set_scale_factor( RES_16 );

    UART1_Write_Text( "Compass2 Setup Completed..\r\n" );
}

void update_compas()
{
    compass2_get_all_values( &mx, &my, &mz );
    heading = compass2_get_compass_heading( mx, my, mz );

    if( heading < 0 )
        heading += 360;

    UART1_Write_Text( "Heading: " );
    FloatToStr( heading, compass_text );
    UART1_Write_Text( compass_text );
}

#endif


#if USE_ACCEL

#include "accel3.h"

sbit ACCEL_3_CS  at GPIOD_ODR.B13;
sbit ACCEL_3_INT at GPIOD_IDR.B10;

uint8_t accel_address = 0x18;
xyz_t* my_coords;

void accel_setup()
{
    //I2C
    I2C1_Init_Advanced( 100000, &_GPIO_MODULE_I2C1_PB67 );
    Delay_ms(200);

    //Accel 3 Initialization
    accel3_init( accel_address, accel_mode, d_rate, mode );
}

void accel_update()
{
    my_coords = accel3_get_xyz();

    sprintf( accel_text, " x: %d y: %d z: %d ", my_coords->x_pos, my_coords->y_pos, my_coords->z_pos );
    UART1_Write_Text( accel_text );
    UART1_Write_Text( "\r\n" );
    Delay_ms(30);  
}

#endif


void main()
{
    #if USE_COMPAS

    uint8_t compas_addres = 0x0F;
    bus_mode_t my_mode = I2C;
    heading = 0;

    compas_setup(my_mode, compas_addres);

    #endif

    bool exit = false;

    while (!exit)
    {
        #if USE_COMPAS

        update_compas();

        #endif


        #if USE_ACCEL

        //

        #endif


        #if USE_GPS

        //

        #endif

        
    }
}